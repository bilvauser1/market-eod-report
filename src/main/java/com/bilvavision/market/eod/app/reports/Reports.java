package com.bilvavision.market.eod.app.reports;

public interface Reports {
	
	public void getEodReport();

	public void getEodReport(String reportName, String localFolderName);

	public void getEodReports(String reportName, String localFolderName, String fromDate, String toDate) throws InterruptedException;
}
