package com.bilvavision.market.eod.app.repositories;

import com.bilvavision.market.eod.app.db.MsSQLUtility;
import com.bilvavision.market.eod.app.utils.DateUtil;
import com.bilvavision.market.eod.app.utils.QueryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class HolidaysRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(HolidaysRepository.class);

    public void loadHolidays() throws SQLException {
        MsSQLUtility db = MsSQLUtility.createConnection();
        ResultSet resultSet = db.query(QueryUtil.getHolidaysListSelectQuery());
        while(resultSet.next()) {
            DateUtil.holidays.add(resultSet.getDate("DATE").toLocalDate());
        }
        LOGGER.info("Loaded Holidays List");
        db.closeConnection();
    }

}
