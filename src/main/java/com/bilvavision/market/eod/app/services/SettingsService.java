package com.bilvavision.market.eod.app.services;

import com.bilvavision.market.eod.app.repositories.SettingsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Map;

@Service
public class SettingsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SettingsService.class);

    @Autowired
    private SettingsRepository settingsRepository;

    public Map<String, String> getCredentials() {
        Map<String, String> credentials = null;
        try {
            LOGGER.info("----- Loading Credentials -----");
            credentials = settingsRepository.loadCredentials();
        } catch (SQLException e) {
            LOGGER.error("Error in SettingsService --> loadCredentials ", e);
        }
        return credentials;
    }
}
