package com.bilvavision.market.eod.app.repositories;

import com.bilvavision.market.eod.app.db.MsSQLUtility;
import com.bilvavision.market.eod.app.enums.MarketSourceEnum;
import com.bilvavision.market.eod.app.enums.ReportNamesEnum;
import com.bilvavision.market.eod.app.utils.DateUtil;
import com.bilvavision.market.eod.app.utils.QueryUtil;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Repository
public class NseDataRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(NseDataRepository.class);

	public void exportToBhav(List<CSVRecord> records, Map<String, Long> instrumentTokens) throws SQLException {
		MsSQLUtility db = MsSQLUtility.createConnection();
		String reportDate = records.get(1).get(10);
		LOGGER.info("Started inserting records in batch for the date : "+reportDate);
		int iCount = db.executeBatchInsertionForNseBhavCopyReport(records, instrumentTokens);
		db.insert(QueryUtil.getLogQuery(MarketSourceEnum.NSE.name(), records.size() - 1, iCount, reportDate, ReportNamesEnum.Bhavcopy.name()));
		LOGGER.info("Inserted log record for the date : "+reportDate);
		LOGGER.info("Calling Day Equities Indicator Store Procedure for the date : "+reportDate);
		db.callStoreProcedure(QueryUtil.getDayScandalIndicatorStoreProcedure(reportDate));
		db.closeConnection();
	}

	public void exportToIndices(List<CSVRecord> records, Map<String, Long> instrumentTokens) throws SQLException {
		MsSQLUtility db = MsSQLUtility.createConnection();
		String reportDate = DateUtil.convertStringToLocalDate(records.get(1).get(1).replace("/", "-"), DateUtil.DD_MM_YYYY).toString();
		LOGGER.info("Started inserting records in Indices for "+reportDate);
		int iCount = db.executeBatchInsertionForNseIndicesReport(records, instrumentTokens);
		db.insert(QueryUtil.getLogQuery(MarketSourceEnum.NSE.name(), records.size() - 1, iCount, reportDate, ReportNamesEnum.Indices.name()));
		LOGGER.info("Inserted log record for the date : "+reportDate);
		LOGGER.info("Calling Day Indices Indicator Store Procedure for the date : "+reportDate);
		db.callStoreProcedure(QueryUtil.getDayIndicesIndicatorStoreProcedure(reportDate));
		db.closeConnection();
	}

	public void callWeeklyEquityIndicator() throws SQLException {
		String startDate = "";
		String endDate = "";
		MsSQLUtility db = MsSQLUtility.createConnection();
		ResultSet resultSet = db.query(QueryUtil.getSettingSelectQuery("WEEKLY_STORE_PROCEDURE"));
		while(resultSet.next()) {
			if("START_DATE".equals(resultSet.getString("ATTRIBUTE_NAME"))) {
				startDate = resultSet.getString("ATTRIBUTE_VALUE");
			}

			if("END_DATE".equals(resultSet.getString("ATTRIBUTE_NAME"))) {
				endDate = resultSet.getString("ATTRIBUTE_VALUE");
			}
		}
		if(DateUtil.convertStringToLocalDate(endDate, DateUtil.YYYY_MM_DD).isEqual(LocalDate.now())) {
			LOGGER.info("Is today equities last day of the week : true");
			db.callStoreProcedure(QueryUtil.getWeeklyScandalIndicatorStoreProcedure(startDate, endDate));
		}else {
			LOGGER.info("Is today equities last day of the week : false");
		}
		db.closeConnection();
	}

	public void callWeeklyIndicesIndicator() throws SQLException {
		String startDate = "";
		String endDate = "";
		MsSQLUtility db = MsSQLUtility.createConnection();
		ResultSet resultSet = db.query(QueryUtil.getSettingSelectQuery("WEEKLY_STORE_PROCEDURE"));
		while(resultSet.next()) {
			if("START_DATE".equals(resultSet.getString("ATTRIBUTE_NAME"))) {
				startDate = resultSet.getString("ATTRIBUTE_VALUE");
			}

			if("END_DATE".equals(resultSet.getString("ATTRIBUTE_NAME"))) {
				endDate = resultSet.getString("ATTRIBUTE_VALUE");
			}
		}
		if(DateUtil.convertStringToLocalDate(endDate, DateUtil.YYYY_MM_DD).isEqual(LocalDate.now())) {
			LOGGER.info("Is today indices last day of the week : true");
			db.callStoreProcedure(QueryUtil.getWeeklyIndicesIndicatorStoreProcedure(startDate, endDate));
		}else {
			LOGGER.info("Is today indices last day of the week : false");
		}
		db.closeConnection();
	}

	public void updateSettingsForWeeklyStoreProc(LocalDate localDate) throws SQLException {
		MsSQLUtility db = MsSQLUtility.createConnection();
		String startDate = DateUtil.getStartDate(localDate).toString();
		String endDate = DateUtil.getEndDate(localDate).toString();
		db.update(QueryUtil.getUpdateSettingQuery("WEEKLY_STORE_PROCEDURE", "START_DATE", startDate));
		db.update(QueryUtil.getUpdateSettingQuery("WEEKLY_STORE_PROCEDURE", "END_DATE", endDate));
		db.closeConnection();
	}
}
