package com.bilvavision.market.eod.app.constants;

public class NseConstants {

	public static final String NSE_EOD_BHAVCOPY_REPORT_HEADER[] = {"SYMBOL", "SERIES", "OPEN", "HIGH", "LOW", "CLOSE", "LAST", "PREVCLOSE", "TOTTRDQTY", "TOTTRDVAL", "TIMESTAMP", "TOTALTRADES", "ISIN"};

	public static final String NSE_EOD_INDICES_REPORT_HEADER[] = {"INDEX NAME", "INDEX DATE", "OPEN INDEX VALUE", "HIGH INDEX VALUE", "LOW INDEX VALUE", "CLOSING INDEX VALUE", "POINTS CHANGE", "CHANGE(%)", "VOLUME", "TURNOVER (RS. CR.)", "P/E", "P/B", "DIV YIELD"};
}
