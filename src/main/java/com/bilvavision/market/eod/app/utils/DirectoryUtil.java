package com.bilvavision.market.eod.app.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class DirectoryUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DirectoryUtil.class);
	
	private static File file = null;
	
	public static void createDir(String dirName) {
		file = new File(dirName);
		if(file.exists()) {
			File[] files = file.listFiles();
			for (File file : files) {
				file.delete();
			}
			file.delete();
			file.mkdirs();
			LOGGER.info("Cleared all files in "+ dirName);
		}else {
			file.mkdirs();
		}
	}
	
	public static void deleteDir(String dirName) {
		file = new File(dirName);
		if(file.exists()) {
			file.delete();
			LOGGER.info("Deleted file/dir "+ file.getName());
		}
	}
	
}
