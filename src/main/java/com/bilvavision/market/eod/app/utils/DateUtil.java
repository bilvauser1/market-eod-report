package com.bilvavision.market.eod.app.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DateUtil {

	public static final String DD_MM_YYYY = "dd-MM-yyyy";
	public static final String DD_MMM_YYYY = "dd-MMM-yyyy";
	public static final String YYYY_MM_DD = "yyyy-MM-dd";

	public static List<LocalDate> holidays = new ArrayList<>();

	public static LocalDate convertStringToLocalDate(String date, String dateFormate) {
		DateTimeFormatter formatter = new DateTimeFormatterBuilder().parseCaseInsensitive().appendPattern(dateFormate).toFormatter(Locale.ENGLISH);
		return LocalDate.parse(date.toLowerCase(), formatter);
	}
	
	public static Date convertStringToDate(String date, String dateFormate) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat();
		return formatter.parse(date);
	}
	
	public static String convertLocalDateToString(LocalDate date, String dateFormate) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormate);
		return date.format(formatter);
	}
	
	public static long getDaysBetweenDates(String fromDate, String toDate) {
		return ChronoUnit.DAYS.between(convertStringToLocalDate(fromDate, DD_MM_YYYY), convertStringToLocalDate(toDate, DD_MM_YYYY));
	}
	
	public static int getDaysBetweenDates(Date fromDate, Date toDate) {
		return 0;
	}
	
	public static boolean isWeekend(LocalDate date) {
		DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
		switch (dayOfWeek) {
		case SATURDAY:
		case SUNDAY:
			return true;
		default:
			return false;
		}
	}
	
	public static boolean isHolidays(LocalDate date) {
		return holidays.contains(date);
	}

	public static boolean isMonday(LocalDate date) {
		DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
		switch (dayOfWeek) {
			case MONDAY:
				return true;
			default:
				return false;
		}
	}

	public static boolean isFriday(LocalDate date) {
		DayOfWeek dayOfWeek = DayOfWeek.of(date.get(ChronoField.DAY_OF_WEEK));
		switch (dayOfWeek) {
			case FRIDAY:
				return true;
			default:
				return false;
		}
	}

	public static LocalDate getStartDate(LocalDate date) {
		int count = 1;
		date = date.plusDays(1);
		for(int i=0; i<count; i++) {
			if((isFriday(date) && !isHolidays(date)) || (!isFriday(date) && !isHolidays(date))) {
				return date;
			}else {
				count = count + 1;
				date = date.plusDays(1);
			}
		}
		return date;
	}

	public static LocalDate getEndDate(LocalDate date) {
		int count = 1;
		date = date.plusDays(5);
		for(int i=0; i<count; i++) {
			if((isFriday(date) && !isHolidays(date)) || (!isFriday(date) && !isHolidays(date))) {
				return date;
			}else {
				count = count + 1;
				date = date.minusDays(1);
			}
		}
		return date;
	}
}
