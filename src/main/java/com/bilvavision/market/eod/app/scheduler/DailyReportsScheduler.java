package com.bilvavision.market.eod.app.scheduler;

import com.bilvavision.market.eod.app.constants.NseConstants;
import com.bilvavision.market.eod.app.core.Properties;
import com.bilvavision.market.eod.app.enums.MarketSourceEnum;
import com.bilvavision.market.eod.app.enums.ReportNamesEnum;
import com.bilvavision.market.eod.app.enums.SegmentEnum;
import com.bilvavision.market.eod.app.repositories.InstrumentTokenRepository;
import com.bilvavision.market.eod.app.repositories.NseDataRepository;
import com.bilvavision.market.eod.app.services.NseDataReportService;
import com.bilvavision.market.eod.app.utils.CsvFileReaderUtil;
import com.bilvavision.market.eod.app.utils.DateUtil;
import com.bilvavision.market.eod.app.utils.DownloadedFilesUtil;
import com.bilvavision.market.eod.app.utils.UnzipUtil;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.time.LocalDate;
import java.util.List;

@Component
public class DailyReportsScheduler {

    private static final Logger LOGGER = LoggerFactory.getLogger(DailyReportsScheduler.class);

    @Autowired
    private NseDataReportService nseReportService;

    @Autowired
    private NseDataRepository nseRepository;

    @Autowired
    private InstrumentTokenRepository instrumentTokenRepository;

    @Autowired
    private Properties properties;

    @Scheduled(cron = "0 35 18 * * MON-FRI")
    public void getDailyNseBhavcopyReport() {
        String filePath = null;
        try {
            LocalDate date = LocalDate.now();
            if(!DateUtil.isWeekend(date) && !DateUtil.isHolidays(date)) {
                LOGGER.info("######################### Started ################################");
                nseReportService.getNseBhavReport();
                filePath = DownloadedFilesUtil.getDownloadPath(ReportNamesEnum.Bhavcopy.name(), MarketSourceEnum.NSE.name());
                if(!ObjectUtils.isEmpty(filePath)) {
                    UnzipUtil.unzip(filePath, properties.getNseEodBhavcopyReportFolder());
                    filePath = properties.getNseEodBhavcopyReportFolder() + filePath;
                    List<CSVRecord> records = CsvFileReaderUtil.readCsvFile(filePath.substring(0, filePath.length() - 4), NseConstants.NSE_EOD_BHAVCOPY_REPORT_HEADER);
                    nseRepository.exportToBhav(records, instrumentTokenRepository.getInstrumentTokens(SegmentEnum.NSE.name()));
                }else {
                    LOGGER.info("Today is holiday..");
                }
                LOGGER.info("######################### Completed ################################");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Scheduled(cron = "0 05 18 * * MON-FRI")
    public void getDailyNseIndicesReport() {
        String filePath = null;
        try {
            LOGGER.info("######################### Started ################################");
            nseReportService.getNseIndicesReport();
            filePath = DownloadedFilesUtil.getDownloadPath(ReportNamesEnum.Indices.name(), MarketSourceEnum.NSE.name());
            if(!ObjectUtils.isEmpty(filePath)) {
                filePath = properties.getNseEodIndicesReportFolder() + filePath;
                List<CSVRecord> records = CsvFileReaderUtil.readCsvFile(filePath, NseConstants.NSE_EOD_INDICES_REPORT_HEADER);
                nseRepository.exportToIndices(records, instrumentTokenRepository.getInstrumentTokens(SegmentEnum.INDICES.name()));
            }else {
                LOGGER.info("Today is holiday..");
            }
            LOGGER.info("######################### Completed ################################");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
