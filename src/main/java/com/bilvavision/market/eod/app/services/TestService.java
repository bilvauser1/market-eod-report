package com.bilvavision.market.eod.app.services;

import com.bilvavision.market.eod.app.constants.QueryConstant;
import com.bilvavision.market.eod.app.db.MsSQLUtility;
import com.bilvavision.market.eod.app.repositories.NseDataRepository;
import com.bilvavision.market.eod.app.utils.DateUtil;
import com.bilvavision.market.eod.app.utils.QueryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.time.LocalDate;

@Service
public class TestService {

    @Autowired
    private NseDataRepository nseRepository;

    public void weeklySPCall() {
        LocalDate date = DateUtil.convertStringToLocalDate("18-11-2018", DateUtil.DD_MM_YYYY);
        try {
            MsSQLUtility db = MsSQLUtility.createConnection();
            String startDate = "";
            String endDate = "";
            for (int i=0; i < 4; i++) {
                System.out.println("----------------------------- Start "+i+" -------------------------------------");
                ResultSet resultSet = db.query(QueryUtil.getSettingSelectQuery("WEEKLY_STORE_PROCEDURE"));
                while(resultSet.next()) {
                    if("START_DATE".equals(resultSet.getString("ATTRIBUTE_NAME"))) {
                        startDate = resultSet.getString("ATTRIBUTE_VALUE");
                    }

                    if("END_DATE".equals(resultSet.getString("ATTRIBUTE_NAME"))) {
                        endDate = resultSet.getString("ATTRIBUTE_VALUE");
                    }
                }
                System.out.println(startDate+" - "+endDate);
                db.callStoreProcedure(QueryUtil.getWeeklyScandalIndicatorStoreProcedure(startDate, endDate));
                db.callStoreProcedure(QueryUtil.getWeeklyIndicesIndicatorStoreProcedure(startDate, endDate));
                date = date.plusDays(7);
                nseRepository.updateSettingsForWeeklyStoreProc(date);
                System.out.println("----------------------------- End "+i+"-------------------------------------");
            }
            db.closeConnection();
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            System.exit(1);
        }
    }

    public void daySPCall() {
        long noOfDays = DateUtil.getDaysBetweenDates("21-11-2018", "19-12-2018");
        LocalDate date = DateUtil.convertStringToLocalDate("21-NOV-2018", DateUtil.DD_MMM_YYYY);
        try {
            MsSQLUtility db = MsSQLUtility.createConnection();
            for (int i = 0; i <= noOfDays; i++) {
                if(!DateUtil.isWeekend(date) && !DateUtil.isHolidays(date)) {
                    //System.out.println("----------------------------- Start "+i+"  -------------------------------------");
                    System.out.println(QueryConstant.DAY_SCANDAL_INDICATORS_STORE_PROC.replace("#REPORT_DATE#", date.toString())+";");
                    db.callStoreProcedure(QueryUtil.getDayScandalIndicatorStoreProcedure(DateUtil.convertLocalDateToString(date, DateUtil.DD_MMM_YYYY)));
                    db.callStoreProcedure(QueryUtil.getDayIndicesIndicatorStoreProcedure(DateUtil.convertLocalDateToString(date, DateUtil.DD_MMM_YYYY)));
                    //System.out.println("----------------------------- End -------------------------------------");
                }
                date = date.plusDays(1);
            }
            db.closeConnection();
        }catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.exit(1);
        }
    }

    public void test() {
        long noOfDays = DateUtil.getDaysBetweenDates("01-01-2017", "06-09-2018");
        LocalDate date = DateUtil.convertStringToLocalDate("02-01-2016", DateUtil.DD_MM_YYYY);
        int weekDayCount = 0;
        int weekEndCount = 0;
        for (int i = 0; i <= noOfDays; i++) {
            if (!DateUtil.isWeekend(date) && !DateUtil.isHolidays(date)) {
                //System.out.println(date);
                weekDayCount = weekDayCount + 1;
            }
            if (DateUtil.isFriday(date)) {
                //System.out.println(date);
                weekEndCount = weekEndCount + 1;
            }
            date = date.plusDays(1);
        }
        System.out.println("weekDayCount: " + weekDayCount);
        System.out.println("weekEndCount: " + weekEndCount);
    }
}
