package com.bilvavision.market.eod.app.core;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Properties {

	@Value("${application.nse.eod.bhavcopy.report.download.folder:null}")
	private String nseEodBhavcopyReportFolder;

	@Value("${application.nse.eod.indices.report.download.folder:null}")
	private String nseEodIndicesReportFolder;

	@Value("${database.sql.url:null}")
	private String dbUrl;
	
	@Value("${database.sql.username:null}")
	private String dbUserName;
	
	@Value("${database.sql.password:null}")
	private String dbPassword;
	
	@Value("${database.sql.driver:null}")
	private String dbDrive;

	@Value("${database.sql.batchsize:0}")
	private int dbBatchSize;

	@Value("${application.zerodha.instrument.token.url:null}")
	private String tokenUrl;

	@Value("${application.zerodha.instrument.token.download.folder:null}")
	private String instrumentTokenFolder;

	public String getNseEodBhavcopyReportFolder() {
		return nseEodBhavcopyReportFolder;
	}

	public void setNseEodBhavcopyReportFolder(String nseEodBhavcopyReportFolder) {
		this.nseEodBhavcopyReportFolder = nseEodBhavcopyReportFolder;
	}

	public String getNseEodIndicesReportFolder() {
		return nseEodIndicesReportFolder;
	}

	public void setNseEodIndicesReportFolder(String nseEodIndicesReportFolder) {
		this.nseEodIndicesReportFolder = nseEodIndicesReportFolder;
	}

	public String getDbUrl() {
		return dbUrl;
	}

	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}

	public String getDbUserName() {
		return dbUserName;
	}

	public void setDbUserName(String dbUserName) {
		this.dbUserName = dbUserName;
	}

	public String getDbPassword() {
		return dbPassword;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}

	public String getDbDrive() {
		return dbDrive;
	}

	public void setDbDrive(String dbDrive) {
		this.dbDrive = dbDrive;
	}

	public int getDbBatchSize() {
		return dbBatchSize;
	}

	public void setDbBatchSize(int dbBatchSize) {
		this.dbBatchSize = dbBatchSize;
	}

	public String getTokenUrl() {
		return tokenUrl;
	}

	public void setTokenUrl(String tokenUrl) {
		this.tokenUrl = tokenUrl;
	}

	public String getInstrumentTokenFolder() {
		return instrumentTokenFolder;
	}

	public void setInstrumentTokenFolder(String instrumentTokenFolder) {
		this.instrumentTokenFolder = instrumentTokenFolder;
	}
}
