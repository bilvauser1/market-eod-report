package com.bilvavision.market.eod.app.core.domain;

public class InstrumentToken {

    private long instrumentToken;
    private long exchangeToken;
    private String tradingSymbol;
    private String name;
    private String instrumentType;
    private String segment;
    private String exchange;

    public long getInstrumentToken() {
        return instrumentToken;
    }

    public void setInstrumentToken(long instrumentToken) {
        this.instrumentToken = instrumentToken;
    }

    public long getExchangeToken() {
        return exchangeToken;
    }

    public void setExchangeToken(long exchangeToken) {
        this.exchangeToken = exchangeToken;
    }

    public String getTradingSymbol() {
        return tradingSymbol;
    }

    public void setTradingSymbol(String tradingSymbol) {
        this.tradingSymbol = tradingSymbol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(String instrumentType) {
        this.instrumentType = instrumentType;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }
}
