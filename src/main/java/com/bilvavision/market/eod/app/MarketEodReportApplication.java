package com.bilvavision.market.eod.app;

import java.io.IOException;
import java.sql.SQLException;

import com.bilvavision.market.eod.app.scheduler.InstrumentTokenScheduler;
import com.bilvavision.market.eod.app.services.HistoricalReportsService;
import com.bilvavision.market.eod.app.services.TestService;
import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.bilvavision.market.eod.app.scheduler.DailyReportsScheduler;

@SpringBootApplication
@EnableScheduling
public class MarketEodReportApplication {

	public static void main(String[] args) throws BeansException, SQLException, IOException {
		SpringApplication.run(MarketEodReportApplication.class, args);
	}
}
