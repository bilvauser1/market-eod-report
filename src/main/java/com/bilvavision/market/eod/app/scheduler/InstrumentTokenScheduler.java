package com.bilvavision.market.eod.app.scheduler;

import com.bilvavision.market.eod.app.constants.Constants;
import com.bilvavision.market.eod.app.core.Properties;
import com.bilvavision.market.eod.app.repositories.InstrumentTokenRepository;
import com.bilvavision.market.eod.app.services.DownloaderService;
import com.bilvavision.market.eod.app.services.SettingsService;
import com.bilvavision.market.eod.app.utils.CsvFileReaderUtil;
import com.bilvavision.market.eod.app.utils.DirectoryUtil;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Component
public class InstrumentTokenScheduler {

    @Autowired
    private DownloaderService service;

    @Autowired
    private SettingsService settingsService;

    @Autowired
    private Properties properties;

    @Autowired
    private InstrumentTokenRepository instrumentTokenRepository;

    @Scheduled(cron = "0 30 17 * * MON-FRI")
    public void loadInstrumentTokens() throws SQLException, IOException {
        DirectoryUtil.createDir(properties.getInstrumentTokenFolder());
        String fileName = properties.getInstrumentTokenFolder().concat("/instrument-tokens.csv");
        service.download(getUrl(), null, fileName);
        List<CSVRecord> records = CsvFileReaderUtil.readCsvFile(fileName, Constants.INSTRUMENT_HEADER_HEADER);
        instrumentTokenRepository.truncateInstrumentTokens();
        instrumentTokenRepository.exportToInstrumentTokens(records);
    }

    private String getUrl() {
        Map<String, String> credentials = settingsService.getCredentials();
        return properties.getTokenUrl().concat("?api_key=").concat(credentials.get("API_KEY"));
    }
}
