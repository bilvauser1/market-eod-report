package com.bilvavision.market.eod.app.repositories;

import com.bilvavision.market.eod.app.db.MsSQLUtility;
import com.bilvavision.market.eod.app.utils.QueryUtil;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class InstrumentTokenRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(NseDataRepository.class);

    public void truncateInstrumentTokens() throws SQLException {
        MsSQLUtility db = MsSQLUtility.createConnection();
        LOGGER.info("Truncating InstrumentTokens");
        db.update(QueryUtil.truncateInstrumentTokens());
        db.closeConnection();
    }

    public void exportToInstrumentTokens(List<CSVRecord> records) throws SQLException {
        MsSQLUtility db = MsSQLUtility.createConnection();
        LOGGER.info("Started inserting InstrumentTokens records in batch");
        int iCount = db.executeBatchInsertionForInstrumentToken(records);
        db.closeConnection();
    }

    public Map<String, Long> getInstrumentTokens(String segment) throws SQLException {
        MsSQLUtility db = MsSQLUtility.createConnection();
        LOGGER.info("getting InstrumentTokens");
        ResultSet resultSet = db.query(QueryUtil.getInstrumentTokensSelectQuery(segment));
        Map<String, Long> instrumentTokens = new HashMap<>();
        while(resultSet.next()) {
            instrumentTokens.put(resultSet.getString("TRADING_SYMBOL"), resultSet.getLong("INSTRUMENT_TOKEN"));
        }
        db.closeConnection();
        return instrumentTokens;
    }
}
