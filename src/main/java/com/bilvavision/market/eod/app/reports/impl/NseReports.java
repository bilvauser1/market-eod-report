package com.bilvavision.market.eod.app.reports.impl;

import com.bilvavision.market.eod.app.reports.Reports;
import com.bilvavision.market.eod.app.services.DownloaderService;
import com.bilvavision.market.eod.app.utils.DateUtil;
import com.bilvavision.market.eod.app.utils.DirectoryUtil;
import com.bilvavision.market.eod.app.utils.DownloadedFilesUtil;
import com.bilvavision.market.eod.app.utils.UrlUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class NseReports implements Reports {

	private static final Logger LOGGER = LoggerFactory.getLogger(NseReports.class);
	
	@Autowired
	private DownloaderService downloaderService;

	@Override
	public void getEodReport() {
		
	}

	@Override
	public void getEodReport(String reportName, String localFolderName) {
		LocalDate date = LocalDate.now();
		if(!DateUtil.isWeekend(date) && !DateUtil.isHolidays(date)) {
			LOGGER.info("Getting NSE "+reportName+" Report for the day : "+date);
			DirectoryUtil.createDir(localFolderName);
			String url = UrlUtil.getURLAddress(date, reportName);
			String fileName = url.substring(url.lastIndexOf("/") + 1);
			String absoluteFilePath = localFolderName + "/" + fileName;
			boolean isDownloaded = downloaderService.download(url, null, absoluteFilePath);
			if(isDownloaded) {
				DownloadedFilesUtil.downloadedFileName.put(reportName, fileName);
			}
		}
	}

	@Override
	public void getEodReports(String reportName, String localFolderName, String fromDate, String toDate) {
		long noOfDays = DateUtil.getDaysBetweenDates(fromDate, toDate);
		LocalDate date = DateUtil.convertStringToLocalDate(fromDate, DateUtil.DD_MM_YYYY);
		List<String> filePaths = new ArrayList<>((int)noOfDays);
		DirectoryUtil.createDir(localFolderName);
		for (int i = 0; i <= noOfDays; i++) {
			if(!DateUtil.isWeekend(date) && !DateUtil.isHolidays(date)) {
				LOGGER.info("Getting NSE "+reportName+" Report for the day : "+date);
				String url = UrlUtil.getURLAddress(date, reportName);
				String fileName = url.substring(url.lastIndexOf("/") + 1);
				String absoluteFilePath = localFolderName + "/" + fileName;
				boolean isDownloaded = downloaderService.download(url, null, absoluteFilePath);
				if(isDownloaded) {
					filePaths.add(fileName);
				}
			}
			date = date.plusDays(1);
		}
		
		if(!ObjectUtils.isEmpty(filePaths)) {
			DownloadedFilesUtil.downloadedFileNames.put(reportName, filePaths);
		}
		
	}
}
