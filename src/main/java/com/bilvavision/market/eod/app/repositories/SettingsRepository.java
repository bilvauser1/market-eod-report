package com.bilvavision.market.eod.app.repositories;

import com.bilvavision.market.eod.app.db.MsSQLUtility;
import com.bilvavision.market.eod.app.utils.QueryUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Component
public class SettingsRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(SettingsRepository.class);
    public static final String CREDENTIALS = "CREDENTIALS";
    public static final String ATTRIBUTE_NAME = "Attribute_Name";
    public static final String ATTRIBUTE_VALUE = "Attribute_Value";

    public Map<String, String> loadCredentials() throws SQLException {
        MsSQLUtility db = MsSQLUtility.createConnection();
        ResultSet resultSet = db.query(QueryUtil.getSettingQueryBySettingName(CREDENTIALS));
        Map<String, String> credentials = new HashMap<>();
        while(resultSet.next()) {
            credentials.put(resultSet.getString(ATTRIBUTE_NAME), resultSet.getString(ATTRIBUTE_VALUE));
        }
        LOGGER.info("----- Loaded Credentials -----");
        db.closeConnection();
        return credentials;
    }
}
