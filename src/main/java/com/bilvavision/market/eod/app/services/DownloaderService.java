package com.bilvavision.market.eod.app.services;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Map;

@Service
public class DownloaderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DownloaderService.class);

    public boolean download(String url, Map<String, String> headers, String absoluteFilePath) {
        OutputStream out = null;
        Response response = null;
        OkHttpClient client = null;
        boolean isDownloaded = false;
        LOGGER.info("Calling URL : "+url+" to download..");
        try {
            client = new OkHttpClient();
            out = new BufferedOutputStream(new FileOutputStream(absoluteFilePath));
            response = client.newCall(generateGETRequest(url, headers)).execute();
            LOGGER.info("Downloading "+absoluteFilePath+" from URL : "+url+" Started");
            if(!ObjectUtils.isEmpty(response) && response.isSuccessful()) {
                out.write(response.body().bytes());
                out.close();
                LOGGER.info("Downloaded "+absoluteFilePath+" from URL : "+url+" Successfully");
                isDownloaded = true;
            }else {
                LOGGER.info("Unable to download "+absoluteFilePath+ " from url "+url);
            }
        } catch (Exception e) {
            LOGGER.error("Error Occurred : ", e);
            download(url, headers, absoluteFilePath);
        }
        return isDownloaded;
    }

    private Request generateGETRequest(String url, Map<String, String> headers) {
        Request.Builder builder = new Request.Builder()
                .url(url)
                .addHeader("Cache-Control", "no-cache");
        if(!ObjectUtils.isEmpty(headers)) {
            headers.keySet().forEach(key -> {
                builder.addHeader(key, headers.get(key));
            });
        }
        return builder.build();
    }
}
