package com.bilvavision.market.eod.app.services;

import com.bilvavision.market.eod.app.core.Properties;
import com.bilvavision.market.eod.app.enums.ReportNamesEnum;
import com.bilvavision.market.eod.app.reports.impl.NseReports;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NseDataReportService {

	private static final Logger LOGGER = LoggerFactory.getLogger(NseDataReportService.class);
	
	@Autowired
	private NseReports nseReports;

	@Autowired
	private Properties properties;

	public void getNseBhavReport() {
		String reportName = ReportNamesEnum.Bhavcopy.name();
		LOGGER.info("Report : "+reportName);
		nseReports.getEodReport(reportName, properties.getNseEodBhavcopyReportFolder());
	}
	
	public void getNseBhavReports(String fromDate, String toDate) throws InterruptedException {
		String reportName = ReportNamesEnum.Bhavcopy.name();
		nseReports.getEodReports(reportName, properties.getNseEodBhavcopyReportFolder(), fromDate, toDate);
	}

	public void getNseIndicesReport() {
		String reportName = ReportNamesEnum.Indices.name();
		LOGGER.info("Report : "+reportName);
		nseReports.getEodReport(reportName, properties.getNseEodIndicesReportFolder());
	}

	public void getNseIndicesReport(String fromDate, String toDate) throws InterruptedException {
		String reportName = ReportNamesEnum.Indices.name();
		nseReports.getEodReports(reportName, properties.getNseEodIndicesReportFolder(), fromDate, toDate);
	}
}
