package com.bilvavision.market.eod.app.utils;

import com.bilvavision.market.eod.app.constants.QueryConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QueryUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(QueryUtil.class);

	public static String getLogQuery(String source, int totalRec, int insRec, String reportDate, String reportName) {
		LOGGER.info("Prepareing log table query");
		String query = QueryConstant.LOG_DATA_INSERTION_QUERY
				.replace("#MARKET_SOURCE#", source)
				.replace("#NO_OF_RECORDS#", String.valueOf(totalRec))
				.replace("#RECORDS_INSERTED#", String.valueOf(insRec))
				.replace("#REPORT_DATE#", reportDate)
				.replace("#REPORT_NAME#", reportName);
		return query;
	}

	public static String getDayScandalIndicatorStoreProcedure(String reportDate) {
		String query = QueryConstant.DAY_SCANDAL_INDICATORS_STORE_PROC.replace("#REPORT_DATE#", DateUtil.convertStringToLocalDate(reportDate, DateUtil.DD_MMM_YYYY).toString());
		return query;
	}

	public static String getDayIndicesIndicatorStoreProcedure(String reportDate) {
		String query = QueryConstant.DAY_INDICES_INDICATORS_STORE_PROC.replace("#REPORT_DATE#", reportDate);
		return query;
	}

	public static String getWeeklyIndicesIndicatorStoreProcedure(String fromDate, String toDate) {
		String query = QueryConstant.WEEKLY_INDICES_INDICATORS_STORE_PROC
				.replace("#FROM_DATE#", fromDate)
				.replace("#TO_DATE#", toDate);
		return query;
	}

	public static String getWeeklyScandalIndicatorStoreProcedure(String fromDate, String toDate) {
		String query = QueryConstant.WEEKLY_SCANDAL_INDICATORS_STORE_PROC
				.replace("#FROM_DATE#", fromDate)
				.replace("#TO_DATE#", toDate);
		return query;
	}

	public static String getUpdateSettingQuery(String settingName, String attName, String attValue) {
		String query = QueryConstant.UPDATE_SETTING
				.replace("#SETTING_NAME#", settingName)
				.replace("#ATTRIBUTE_VALUE#", attValue)
				.replace("#ATTRIBUTE_NAME#", attName);
		return query;
	}

	public static String getSettingSelectQuery(String settingName) {
		String query = QueryConstant.SELECT_SETTING
				.replace("#SETTING_NAME#", settingName);
		return query;
	}

	public static String getHolidaysListSelectQuery() {
		return QueryConstant.SELECT_HOLIDAYS_LIST;
	}

	public static String getSettingQueryBySettingName(String settingName) {
		String query = QueryConstant.SELECT_SETTING
				.replace("#SETTING_NAME#", settingName);
		return query;
	}

	public static String insertInstrumentTokens() {
		return QueryConstant.INSTRUMENT_TOKENS_INSERTION_QUERY;
	}

	public static String truncateInstrumentTokens() {
		return QueryConstant.INSTRUMENT_TOKENS_TRUNCATE_QUERY;
	}

	public static String getInstrumentTokensSelectQuery(String segment) {
		return QueryConstant.NSE_SEGMENT_INSTRUMENT_TOKENS_SELECT_QUERY.replace("#SEGMENT#", segment);
	}
}
