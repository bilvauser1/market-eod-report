package com.bilvavision.market.eod.app.utils;

import com.bilvavision.market.eod.app.enums.MarketSourceEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DownloadedFilesUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(DownloadedFilesUtil.class);

	public static final Map<String, String> downloadedFileName = new HashMap<>();
	public static final Map<String, List<String>> downloadedFileNames = new HashMap<>();

	public static String getDownloadPath(String reportName, String source) {
		LOGGER.info("Getting Download Dir path for report : "+reportName);
		if(downloadedFileName.containsKey(reportName)) {
			if(MarketSourceEnum.NSE.name().equals(source)) {
				return "/"+ downloadedFileName.get(reportName);
			}
		}
		return null;
	}

	public static List<String> getDownloadedFilePaths(String reportName, String source) {
		if(downloadedFileNames.containsKey(reportName)) {
			if(MarketSourceEnum.NSE.name().equals(source) && !ObjectUtils.isEmpty(downloadedFileNames.get(reportName))) {
				List<String> filePaths = new ArrayList<>();
				for (String name : downloadedFileNames.get(reportName)) {
					filePaths.add("/"+ name);
				}
				return filePaths;
			}
		}
		return null;
	}
}
