package com.bilvavision.market.eod.app.scheduler;

import com.bilvavision.market.eod.app.repositories.NseDataRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.time.LocalDate;

@Component
public class WeeklyIndicatorsScheduler {

    private static final Logger LOGGER = LoggerFactory.getLogger(WeeklyIndicatorsScheduler.class);

    @Autowired
    private NseDataRepository nseRepository;

    @Scheduled(cron = "0 30 19 * * MON-FRI")
    public void calculateIndicesWeeklyIndicators() throws SQLException {
        nseRepository.callWeeklyIndicesIndicator();
    }

    @Scheduled(cron = "0 00 19 * * MON-FRI")
    public void calculateEquityWeeklyIndicators() throws SQLException {
        nseRepository.callWeeklyEquityIndicator();
    }

    @Scheduled(cron = "0 50 20 * * SUN")
    public void defineWeekStartAndEndDate() throws SQLException {
        nseRepository.updateSettingsForWeeklyStoreProc(LocalDate.now());
    }
}
