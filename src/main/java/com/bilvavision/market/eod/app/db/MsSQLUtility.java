package com.bilvavision.market.eod.app.db;

import com.bilvavision.market.eod.app.constants.QueryConstant;
import com.bilvavision.market.eod.app.core.Properties;
import com.bilvavision.market.eod.app.core.context.ApplicationContextProvider;
import com.bilvavision.market.eod.app.enums.SegmentEnum;
import com.bilvavision.market.eod.app.utils.DateUtil;
import com.bilvavision.market.eod.app.utils.QueryUtil;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ObjectUtils;

import java.sql.*;
import java.util.List;
import java.util.Map;

public class MsSQLUtility {

    private static final Logger LOGGER = LoggerFactory.getLogger(MsSQLUtility.class);

    private Connection connection;
    private Statement statement;
    private PreparedStatement preparedStatement;
    private static MsSQLUtility db;

    private Properties properties = ApplicationContextProvider.getApplicationContext().getBean(Properties.class);

    private MsSQLUtility() {
        try {
            Class.forName(properties.getDbDrive()).newInstance();
            this.connection = DriverManager.getConnection(properties.getDbUrl(), properties.getDbUserName(), properties.getDbPassword());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static MsSQLUtility createConnection() throws SQLException {
        //if (db == null || db.connection.isClosed()) {
            db = new MsSQLUtility();
       // }
        return db;
    }

    public void closeConnection() throws SQLException {
        if (!connection.isClosed()) {
            connection.close();
            db = null;
            LOGGER.info("opened db connection closed");
        }
    }

    public ResultSet query(String query) throws SQLException {
        statement = connection.createStatement();
        ResultSet res = statement.executeQuery(query);
        LOGGER.info("Query executed successfully.");
        return res;
    }

    public int insert(String insertQuery) throws SQLException {
        connection.setAutoCommit(false);
        statement = connection.createStatement();
        int result = statement.executeUpdate(insertQuery);
        connection.commit();
        LOGGER.info("Insert Query executed successfully.");
        return result;
    }

    public int update(String insertQuery) throws SQLException {
        connection.setAutoCommit(false);
        statement = connection.createStatement();
        int result = statement.executeUpdate(insertQuery);
        connection.commit();
        LOGGER.info("Updated Query executed successfully.");
        return result;
    }

    public int executeBatchInsertionForNseBhavCopyReport(List<CSVRecord> record, Map<String, Long> instrumentTokens) throws SQLException {
        connection.setAutoCommit(false);
        preparedStatement = connection.prepareStatement(QueryConstant.DAILY_NSE_BHAV_DATA_INSERTION_QUERY);
        int count = 0, insertedRecCount = 0;
        int result[] = {};
        for (CSVRecord csvRecord : record) {
            if (count > 0 && csvRecord.get(1).equals("EQ")
                    && (instrumentTokens.containsKey(csvRecord.get(0))
                    || instrumentTokens.containsKey(csvRecord.get(0).concat("-").concat(csvRecord.get(1))))) {
                preparedStatement.setString(1, csvRecord.get(0));
                preparedStatement.setString(2, csvRecord.get(1));
                preparedStatement.setFloat(3, Float.valueOf(csvRecord.get(2)));
                preparedStatement.setFloat(4, Float.valueOf(csvRecord.get(3)));
                preparedStatement.setFloat(5, Float.valueOf(csvRecord.get(4)));
                preparedStatement.setFloat(6, Float.valueOf(csvRecord.get(5)));
                preparedStatement.setFloat(7, Float.valueOf(csvRecord.get(6)));
                preparedStatement.setFloat(8, Float.valueOf(csvRecord.get(7)));
                preparedStatement.setInt(9, Integer.valueOf(csvRecord.get(8)));
                preparedStatement.setFloat(10, Float.valueOf(csvRecord.get(9)));
                preparedStatement.setDate(11, Date.valueOf(DateUtil.convertStringToLocalDate(csvRecord.get(10), DateUtil.DD_MMM_YYYY)));
                preparedStatement.setString(12, csvRecord.get(11));
                preparedStatement.setString(13, csvRecord.get(12));
                if(instrumentTokens.get(csvRecord.get(0)) != null && csvRecord.get(1).equals("EQ")) {
                    preparedStatement.setLong(14, instrumentTokens.get(csvRecord.get(0)));
                }else if(instrumentTokens.get(csvRecord.get(0).concat("-").concat(csvRecord.get(1))) != null && !csvRecord.get(1).equals("EQ")) {
                    preparedStatement.setLong(14, instrumentTokens.get(csvRecord.get(0).concat("-").concat(csvRecord.get(1))));
                }else if(instrumentTokens.get(csvRecord.get(0)) != null) {
                    preparedStatement.setLong(14, instrumentTokens.get(csvRecord.get(0)));
                }
                preparedStatement.addBatch();

                if (count % properties.getDbBatchSize() == 0) {
                    result = preparedStatement.executeBatch();
                    insertedRecCount = insertedRecCount + result.length;
                }
            }
            count++;
        }
        result = preparedStatement.executeBatch();
        connection.commit();
        insertedRecCount = insertedRecCount + result.length;
        LOGGER.info("Successfully inserted batch data");
        LOGGER.info("Total no of records inserted Successfully : " + insertedRecCount);
        return insertedRecCount;
    }

    public void callStoreProcedure(String procedure) throws SQLException {
        connection.setAutoCommit(false);
        CallableStatement statement = connection.prepareCall(procedure);
        statement.execute();
        connection.commit();
        LOGGER.info("Store Procedure Execution completed");
    }

    public int executeBatchInsertionForNseIndicesReport(List<CSVRecord> record, Map<String, Long> instrumentTokens) throws SQLException {
        connection.setAutoCommit(false);
        preparedStatement = connection.prepareStatement(QueryConstant.DAILY_NSE_INDICES_DATA_INSERTION_QUERY);
        int count = 0, insertedRecCount = 0;
        int result[] = {};
        for (CSVRecord csvRecord : record) {
            if (count > 0 && (instrumentTokens.containsKey(csvRecord.get(0).toUpperCase()))) {
                preparedStatement.setString(1, csvRecord.get(0).toUpperCase());
                preparedStatement.setDate(2, Date.valueOf(DateUtil.convertStringToLocalDate(csvRecord.get(1).replace("/", "-"), DateUtil.DD_MM_YYYY)));
                preparedStatement.setFloat(3, (!ObjectUtils.isEmpty(csvRecord.get(2)) && !"-".equals(csvRecord.get(2))) ? Float.valueOf(csvRecord.get(2)) : 0);
                preparedStatement.setFloat(4, (!ObjectUtils.isEmpty(csvRecord.get(3)) && !"-".equals(csvRecord.get(3))) ? Float.valueOf(csvRecord.get(3)) : 0);
                preparedStatement.setFloat(5, (!ObjectUtils.isEmpty(csvRecord.get(4)) && !"-".equals(csvRecord.get(4))) ? Float.valueOf(csvRecord.get(4)) : 0);
                preparedStatement.setFloat(6, (!ObjectUtils.isEmpty(csvRecord.get(5)) && !"-".equals(csvRecord.get(5))) ? Float.valueOf(csvRecord.get(5)) : 0);
                preparedStatement.setFloat(7, (!ObjectUtils.isEmpty(csvRecord.get(6)) && !"-".equals(csvRecord.get(6))) ? Float.valueOf(csvRecord.get(6)) : 0);
                preparedStatement.setFloat(8, (!ObjectUtils.isEmpty(csvRecord.get(7)) && !"-".equals(csvRecord.get(7))) ? Float.valueOf(csvRecord.get(7)) : 0);
                preparedStatement.setLong(9, (!ObjectUtils.isEmpty(csvRecord.get(8)) && !"-".equals(csvRecord.get(8))) ? Long.valueOf(csvRecord.get(8)) : 0);
                preparedStatement.setFloat(10, (!ObjectUtils.isEmpty(csvRecord.get(9)) && !"-".equals(csvRecord.get(9))) ? Float.valueOf(csvRecord.get(9)) : 0);
                preparedStatement.setFloat(11, (!ObjectUtils.isEmpty(csvRecord.get(10)) && !"-".equals(csvRecord.get(10))) ? Float.valueOf(csvRecord.get(10)) : 0);
                preparedStatement.setFloat(12, (!ObjectUtils.isEmpty(csvRecord.get(11)) && !"-".equals(csvRecord.get(11))) ? Float.valueOf(csvRecord.get(11)) : 0);
                preparedStatement.setFloat(13, (!ObjectUtils.isEmpty(csvRecord.get(12)) && !"-".equals(csvRecord.get(12))) ? Float.valueOf(csvRecord.get(12)) : 0);
                preparedStatement.setLong(14, instrumentTokens.get(csvRecord.get(0).toUpperCase()));
                preparedStatement.setString(15, SegmentEnum.INDICES.name());
                preparedStatement.addBatch();

                if (count % properties.getDbBatchSize() == 0) {
                    result = preparedStatement.executeBatch();
                    insertedRecCount = insertedRecCount + result.length;
                }
            }
            count++;
        }
        result = preparedStatement.executeBatch();
        connection.commit();
        insertedRecCount = insertedRecCount + result.length;
        LOGGER.info("Total no of records inserted Successfully : " + insertedRecCount);
        return insertedRecCount;
    }

    public int executeBatchInsertionForInstrumentToken(List<CSVRecord> record) throws SQLException {
        connection.setAutoCommit(false);
        preparedStatement = connection.prepareStatement(QueryUtil.insertInstrumentTokens());
        int count = 0, insertedRecCount = 0;
        int result[] = {};
        for (CSVRecord csvRecord : record) {
            if (count > 0) {
                preparedStatement.setLong(1, Long.valueOf(csvRecord.get(0)));
                preparedStatement.setLong(2, Long.valueOf(csvRecord.get(1)));
                preparedStatement.setString(3, csvRecord.get(2));
                preparedStatement.setString(4, csvRecord.get(3));
                preparedStatement.setString(5, csvRecord.get(9));
                preparedStatement.setString(6, csvRecord.get(10));
                preparedStatement.setString(7, csvRecord.get(11));
                preparedStatement.addBatch();

                if (count % properties.getDbBatchSize() == 0) {
                    result = preparedStatement.executeBatch();
                    insertedRecCount = insertedRecCount + result.length;
                }
            }
            count++;
        }
        result = preparedStatement.executeBatch();
        connection.commit();
        insertedRecCount = insertedRecCount + result.length;
        LOGGER.info("Successfully inserted batch data");
        LOGGER.info("Total no of records inserted Successfully : " + insertedRecCount);
        return insertedRecCount;
    }
}
