package com.bilvavision.market.eod.app.core;

import com.bilvavision.market.eod.app.repositories.HolidaysRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationStartup.class);

    @Autowired
    private HolidaysRepository repository;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        try {
            LOGGER.info("Loading Holidays List");
            repository.loadHolidays();
        } catch (SQLException e) {
            LOGGER.error("Error in ApplicationStartup --> onApplicationEvent ", e);
        }
    }
}
