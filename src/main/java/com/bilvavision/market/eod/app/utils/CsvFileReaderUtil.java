package com.bilvavision.market.eod.app.utils;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class CsvFileReaderUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(CsvFileReaderUtil.class);
	
	public static List<CSVRecord> readCsvFile(String fileName, String [] fileHeaderMapping) throws IOException {
		FileReader fileReader = null;
		CSVParser csvFileParser = null;
		List<CSVRecord> csvParsers = null;
		CSVFormat csvFileFormat = CSVFormat.DEFAULT.withHeader(fileHeaderMapping);
		try {
			LOGGER.info("Started reading csv file : "+fileName);
			fileReader = new FileReader(fileName); 		//initialize FileReader object
			csvFileParser = new CSVParser(fileReader, csvFileFormat);		//initialize CSVParser object			
			csvParsers = csvFileParser.getRecords();			
		} catch (Exception e) {
			LOGGER.error("Error Occoured : ", e);
		}
		return csvParsers;
	}	
}
