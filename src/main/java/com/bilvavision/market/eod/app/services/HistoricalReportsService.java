package com.bilvavision.market.eod.app.services;

import com.bilvavision.market.eod.app.constants.NseConstants;
import com.bilvavision.market.eod.app.core.Properties;
import com.bilvavision.market.eod.app.enums.MarketSourceEnum;
import com.bilvavision.market.eod.app.enums.ReportNamesEnum;
import com.bilvavision.market.eod.app.enums.SegmentEnum;
import com.bilvavision.market.eod.app.repositories.InstrumentTokenRepository;
import com.bilvavision.market.eod.app.repositories.NseDataRepository;
import com.bilvavision.market.eod.app.utils.CsvFileReaderUtil;
import com.bilvavision.market.eod.app.utils.DownloadedFilesUtil;
import com.bilvavision.market.eod.app.utils.UnzipUtil;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Map;

@Service
public class HistoricalReportsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(HistoricalReportsService.class);

    @Autowired
    private NseDataReportService nseReportService;

    @Autowired
    private NseDataRepository nseRepository;

    @Autowired
    private InstrumentTokenRepository instrumentTokenRepository;

    @Autowired
    private Properties properties;

    public void getHistoricalNseBhavcopyReportsData() {
        List<String> filePaths = null;
        try {
            nseReportService.getNseBhavReports("12-12-2018", "18-12-2018");
            filePaths = DownloadedFilesUtil.getDownloadedFilePaths(ReportNamesEnum.Bhavcopy.name(), MarketSourceEnum.NSE.name());
            if(!ObjectUtils.isEmpty(filePaths)) {
                Map<String, Long> instrumentTokens = instrumentTokenRepository.getInstrumentTokens(SegmentEnum.NSE.name());
                for (String filePath : filePaths) {
                    UnzipUtil.unzip(filePath, properties.getNseEodBhavcopyReportFolder());
                    filePath = properties.getNseEodBhavcopyReportFolder() + filePath;
                    List<CSVRecord> records = CsvFileReaderUtil.readCsvFile(filePath.substring(0, filePath.length() - 4), NseConstants.NSE_EOD_BHAVCOPY_REPORT_HEADER);
                    nseRepository.exportToBhav(records, instrumentTokens);
                    LOGGER.info("######################### Completed ################################");
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error occurred at class : "+HistoricalReportsService.class+ " --> getHistoricalNseBhavcopyReportsData ", e);
        } finally {
			System.exit(1);
		}
    }

    public void getHistoricalNseIndicesReportsData() {
        List<String> filePaths = null;
        try {
            nseReportService.getNseIndicesReport("12-12-2018", "18-12-2018");
            filePaths = DownloadedFilesUtil.getDownloadedFilePaths(ReportNamesEnum.Indices.name(), MarketSourceEnum.NSE.name());
            if(!ObjectUtils.isEmpty(filePaths)) {
                Map<String, Long> instrumentTokens = instrumentTokenRepository.getInstrumentTokens(SegmentEnum.INDICES.name());
                for (String filePath : filePaths) {
                    filePath = properties.getNseEodIndicesReportFolder() + filePath;
                    List<CSVRecord> records = CsvFileReaderUtil.readCsvFile(filePath, NseConstants.NSE_EOD_INDICES_REPORT_HEADER);
                    nseRepository.exportToIndices(records, instrumentTokens);
                    LOGGER.info("######################### Completed ################################");
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error occurred at class : "+HistoricalReportsService.class+ " --> getHistoricalNseIndicesReportsData ", e);
        }
    }

}
