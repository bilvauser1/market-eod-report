package com.bilvavision.market.eod.app.utils;

import com.bilvavision.market.eod.app.enums.ReportNamesEnum;

import java.time.LocalDate;

public class UrlUtil {

    public static String getURLAddress(LocalDate date, String reportName) {
        int year = date.getYear();
        int month = date.getMonthValue();
        month = month % 100;
        String months[] = {"", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};
        int day = date.getDayOfMonth();
        String address = "";
        if (ReportNamesEnum.Bhavcopy.name().equals(reportName)) {
            String baseAddress = "https://www.nseindia.com/content/historical/EQUITIES";
            address = String.format("%s/%d/%s/cm%02d%s%dbhav.csv.zip", baseAddress, year, months[month], day, months[month], year);
        } else if (ReportNamesEnum.MTO.name().equals(reportName)) {
            String baseAddress = "https://www.nseindia.com/archives/equities/mto";
            address = String.format("%s/MTO_%02d%02d%d.DAT", baseAddress, day, month, year);
        } else if (ReportNamesEnum.FandO.name().equals(reportName)) {
            String baseAddress = "https://www.nseindia.com/content/historical/DERIVATIVES";
            address = String.format("%s/%d/%s/fo%02d%s%dbhav.csv.zip", baseAddress, year, months[month], day, months[month], year);
        } else if (ReportNamesEnum.Indices.name().equals(reportName)) {
            String baseAddress = "https://www.nseindia.com/content/indices";
            address = String.format("%s/ind_close_all_%02d%02d%d.csv", baseAddress, day, month, year);
        }
        return address;
    }

}
