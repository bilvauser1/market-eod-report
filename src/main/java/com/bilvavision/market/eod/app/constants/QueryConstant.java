package com.bilvavision.market.eod.app.constants;

public class QueryConstant {

	public static final String DAILY_NSE_BHAV_DATA_INSERTION_QUERY = "INSERT INTO DAILY_NSE_BHAV_DATA VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	public static final String LOG_DATA_INSERTION_QUERY = "INSERT INTO LOG_DATA (MARKET_SOURCE, NO_OF_RECORDS, RECORDS_INSERTED, REPORT_DATE, REPORT_NAME) VALUES ('#MARKET_SOURCE#', #NO_OF_RECORDS#, #RECORDS_INSERTED#, '#REPORT_DATE#', '#REPORT_NAME#')";

	public static final String DAY_SCANDAL_INDICATORS_STORE_PROC = "EXEC dbo.calculate_day_equity_scandal_indicator '#REPORT_DATE#'";

	public static final String DAY_INDICES_INDICATORS_STORE_PROC = "EXEC dbo.calculate_day_indices_scandal_indicator '#REPORT_DATE#'";

	public static final String DAILY_NSE_INDICES_DATA_INSERTION_QUERY = "INSERT INTO DAILY_NSE_INDICES_DATA VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	public static final String WEEKLY_INDICES_INDICATORS_STORE_PROC = "EXEC dbo.calculate_weekly_indices_scandal_indicator '#FROM_DATE#', '#TO_DATE#'";

	public static final String WEEKLY_SCANDAL_INDICATORS_STORE_PROC = "EXEC dbo.calculate_weekly_equity_scandal_indicator '#FROM_DATE#', '#TO_DATE#'";

	public static final String UPDATE_SETTING = "UPDATE SETTING SET ATTRIBUTE_VALUE = '#ATTRIBUTE_VALUE#' WHERE SETTING_NAME = '#SETTING_NAME#' AND ATTRIBUTE_NAME = '#ATTRIBUTE_NAME#'";

	public static final String SELECT_SETTING = "SELECT * FROM SETTING WHERE SETTING_NAME = '#SETTING_NAME#'";

	public static final String SELECT_HOLIDAYS_LIST = "SELECT * FROM HOLIDAYS_LIST";

	public static final String INSTRUMENT_TOKENS_INSERTION_QUERY = "INSERT INTO INSTRUMENT_TOKENS VALUES (?, ?, ?, ?, ?, ?, ?)";

	public static final String INSTRUMENT_TOKENS_TRUNCATE_QUERY = "TRUNCATE TABLE INSTRUMENT_TOKENS";

	public static final String NSE_SEGMENT_INSTRUMENT_TOKENS_SELECT_QUERY = "SELECT * FROM INSTRUMENT_TOKENS WHERE SEGMENT = '#SEGMENT#'";
}
