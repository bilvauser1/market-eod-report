package com.bilvavision.market.eod.app.enums;

public enum SegmentEnum {

	BSE("BSE"),
	NSE("NSE"),
	CDS_FUT("CDS-FUT"),
	NFO_OPT("NFO-OPT"),
	NFO_FUT("NFO-FUT"),
	MCX("MCX"),
	MCX_OPT("MCX-OPT"),
	INDICES("INDICES"),
	NSE_INDICES("NSE-INDICES"),
	CDS_OPT("CDS-OPT");

	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	SegmentEnum(String description) {
		this.description = description;
	}
}
